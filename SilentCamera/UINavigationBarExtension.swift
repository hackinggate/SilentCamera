//
//  UINavigationBarExtension.swift
//  SilentCamera
//
//  Created by ERU on 2018/06/29.
//  Copyright © 2018 HackingGate. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func setTureBlackStyle() {
        self.barStyle = .black
        self.barTintColor = .black
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = false
    }

}
