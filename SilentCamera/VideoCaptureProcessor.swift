//
//  VideoCaptureProcessor.swift
//  SilentCamera
//
//  Created by ERU on 2018/06/30.
//  Copyright © 2018 HackingGate. All rights reserved.
//

import AVFoundation
import UIKit

class VideoCaptureProcessor: NSObject {
    
    private var backgroundRecordingID: UIBackgroundTaskIdentifier?
    
    private let videoCaptureHandler: (Bool) -> Void

    private let completionHandler: (VideoCaptureProcessor, Error?) -> Void
    
    private var videoURL: URL?
    
    init(with backgroundRecordingID: UIBackgroundTaskIdentifier?,
         videoCaptureHandler: @escaping (Bool) -> Void,
         completionHandler: @escaping (VideoCaptureProcessor, Error?) -> Void) {
        self.backgroundRecordingID = backgroundRecordingID
        self.videoCaptureHandler = videoCaptureHandler
        self.completionHandler = completionHandler
    }
    
    private func cleanUp() {
        if let path = videoURL?.path {
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                } catch {
                    print("Could not remove file at path: \(path)")
                }
            }
            
            if let currentBackgroundRecordingID = backgroundRecordingID {
                backgroundRecordingID = UIBackgroundTaskInvalid
                
                if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
                    UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
                }
            }
        }
        completionHandler(self, nil)
    }
}

extension VideoCaptureProcessor: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        videoCaptureHandler(true)
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        /*
         Note that currentBackgroundRecordingID is used to end the background task
         associated with this recording. This allows a new recording to be started,
         associated with a new UIBackgroundTaskIdentifier, once the movie file output's
         `isRecording` property is back to false — which happens sometime after this method
         returns.
         
         Note: Since we use a unique file path for each recording, a new recording will
         not overwrite a recording currently being saved.
         */
        
        var success = true
        
        if error != nil {
            print("Movie file finishing error: \(String(describing: error))")
            success = (((error! as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
        }
        
        if success {
            videoURL = outputFileURL
            
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(outputFileURL.relativePath) {
                UISaveVideoAtPathToSavedPhotosAlbum(outputFileURL.relativePath, self, #selector(video(_:didFinishSavingWithError:contextInfo:)), nil)
            } else {
                cleanUp()
            }
            
            /*
             // Check authorization status.
             PHPhotoLibrary.requestAuthorization { status in
             if status == .authorized {
             // Save the movie file to the photo library and cleanup.
             PHPhotoLibrary.shared().performChanges({
             let options = PHAssetResourceCreationOptions()
             options.shouldMoveFile = true
             let creationRequest = PHAssetCreationRequest.forAsset()
             creationRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
             }, completionHandler: { success, error in
             if !success {
             print("Could not save movie to photo library: \(String(describing: error))")
             }
             cleanUp()
             }
             )
             } else {
             cleanUp()
             }
             }
             */
        } else {
            cleanUp()
        }
        
        
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        completionHandler(self, error)
    }
    
}
