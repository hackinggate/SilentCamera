//
//  CaptureDeviceInput.swift
//  SilentCamera
//
//  Created by ERU on 2018/07/06.
//  Copyright © 2018 HackingGate. All rights reserved.
//

import UIKit
import AVFoundation

class CaptureDeviceInput: NSObject {
    
    var videoDeviceInput: AVCaptureDeviceInput?

    func configureInputDevice(session: AVCaptureSession, videoDevice: AVCaptureDevice) {

        do {
            let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
            
            session.beginConfiguration()
            
            // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
            if let currentDeviceInput = self.videoDeviceInput {
                session.removeInput(currentDeviceInput)
            }

            if session.canAddInput(videoDeviceInput) {
//                NotificationCenter.default.removeObserver(self, name: .AVCaptureDeviceSubjectAreaDidChange, object: currentVideoDevice)
                
//                NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: videoDeviceInput.device)
                
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
            } else {
                fatalError("Unable to add camera")
            }
            
//            if let connection = movieFileOutput?.connection(with: .video) {
//                if connection.isVideoStabilizationSupported {
//                    connection.preferredVideoStabilizationMode = .auto
//                }
//            }
            
            /*
             Set Live Photo capture and depth data delivery if it is supported. When changing cameras, the
             `livePhotoCaptureEnabled and depthDataDeliveryEnabled` properties of the AVCapturePhotoOutput gets set to NO when
             a video device is disconnected from the session. After the new video device is
             added to the session, re-enable them on the AVCapturePhotoOutput if it is supported.
             */
//            self.photoOutput.isLivePhotoCaptureEnabled = self.photoOutput.isLivePhotoCaptureSupported
//            self.photoOutput.isDepthDataDeliveryEnabled = self.photoOutput.isDepthDataDeliverySupported
            
            session.commitConfiguration()
        } catch {
            print("Error occured while creating video device input: \(error)")
        }
        
    }

}
