//
//  ViewController.swift
//  SilentCamera
//
//  Created by ERU on 2018/06/17.
//  Copyright © 2018 HackingGate. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var captureDevice: AVCaptureDevice?
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var captureInput: AVCaptureDeviceInput?
    var captureOutput: AVCaptureVideoDataOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCamera()
        setupSampleBuffer()
    }
    
    func setupCamera() {
        // Initialize the captureSession object
        captureSession = AVCaptureSession()
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter
        captureDevice = AVCaptureDevice.default(for: .video)
        guard let captureDevice = captureDevice else {
            fatalError("No video device found")
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous deivce object
            captureInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch {
            print(error)
        }
        guard let input = captureInput else { return }
        
        // Set the input devcie on the capture session
        captureSession?.addInput(input)
        
        // Setup output data
        captureOutput = AVCaptureVideoDataOutput()
        guard let captureOutput = captureOutput else {
            fatalError("No output data")
        }
        captureSession?.addOutput(captureOutput)
        
        
        // start session
        captureSession?.startRunning()
    }
    
    func setupSampleBuffer() {
        // Set sample buffer delegate
        captureOutput?.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        
        captureOutput?.alwaysDiscardsLateVideoFrames = true
        
        // deviceをロックして設定
        do {
            try captureDevice?.lockForConfiguration()
            // フレームレート
            captureDevice?.activeVideoMinFrameDuration = CMTimeMake(1, 30)
            captureDevice?.unlockForConfiguration()
        } catch _ {
        }
    }
    
    @IBAction func onTapTakePhoto(_ sender: UIButton) {
        takeStillPhotoFromSampleBuffer()
    }
    
    func takeStillPhotoFromSampleBuffer(){
        if var _:AVCaptureConnection = captureOutput?.connection(with: .video) {
            // Save photo
            guard let image = self.imageView.image else {
                fatalError("No image")
            }
            UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    // [Swift] [iOS] ズーム、ピント調節可能な無音カメラのサンプル
    // https://qiita.com/touyu/items/6fd26a35212e75f98c6b
    
    // 新しいキャプチャの追加で呼ばれる
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // キャプチャしたsampleBufferからUIImageを作成
        let image:UIImage = self.captureImage(sampleBuffer)
        
        // カメラの画像を画面に表示
        DispatchQueue.main.async {
            self.imageView.image = image
        }
    }
    
    // CMSampleBufferをUIImageに変換する
    // https://qiita.com/koki_h/items/91d9bf918df7c5788ffc
    func captureImage(_ sampleBuffer: CMSampleBuffer) -> UIImage {
        
        // サンプルバッファからピクセルバッファを取り出す
        guard let pixelBuffer: CVImageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            fatalError("Unable to get pixel buffer")
        }
        
        // ピクセルバッファをベースにCoreImageのCIImageオブジェクトを作成
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        
        //CIImageからCGImageを作成
        let pixelBufferWidth = CGFloat(CVPixelBufferGetWidth(pixelBuffer))
        let pixelBufferHeight = CGFloat(CVPixelBufferGetHeight(pixelBuffer))
        let imageRect:CGRect = CGRect(x: 0, y: 0, width: pixelBufferWidth, height: pixelBufferHeight)
        let ciContext = CIContext()
        guard let cgimage = ciContext.createCGImage(ciImage, from: imageRect) else {
            fatalError("Unable to get cgimage")
        }
        
        // CGImageからUIImageを作成
        let resultImage = UIImage(cgImage: cgimage, scale: 1.0, orientation: .right)
        return resultImage
    }

}

