//
//  ShutterSoundCanceller.swift
//  SilentCamera
//
//  Created by ERU on 2018/07/01.
//  Copyright © 2018 HackingGate. All rights reserved.
//

import MediaPlayer

class ShutterSoundCanceller {
    static func playShutterInvertSound() {
        if let soundURL = Bundle.main.url(forResource: "photoShutterAntiSound", withExtension: "caf")  {
            do {
                // Play sound even in silent mode
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeMeasurement, options: [])
            } catch {
                print(error)
            }
            do {
                // Temporarily changes the current audio route to speaker
                // TODO: For J and KH only
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            } catch {
                // If category wasn't playAndRecord, route to speaker will fail.
                print(error)
            }
            do {
                // Active
                try AVAudioSession.sharedInstance().setActive(true)
            } catch {
                print(error)
            }
            do {
                let player = try AVAudioPlayer(contentsOf: soundURL)
                player.play()
            } catch {
                print(error)
            }
        }
    }
}
